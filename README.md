# SABD Project2 Flink #

## Introduzione ##

La seguente repository contiene due branch:
  
  * Master: contenente la versione standalone di Flink
  * flinkTest: contenente la versione da cui estrarre il jar da usare nel cluster Flink

### Versione per cluster ###

Nella versione per cluster una volta caricato il jar per far partire l'esecuzione � necessario inserire dei parametri:  

* *1* : per eseguire solamente la query1  
* *2* : per eseguire solamente la query2  
* *3* : per eseguire solamente la query3  
* *all* : per eseguire tutte e tre le query contemporaneamente  

## Developer ##

* Cenciarelli Andrea
* Talone Alberto