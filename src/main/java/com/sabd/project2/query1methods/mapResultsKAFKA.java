package com.sabd.project2.query1methods;

import com.google.gson.Gson;
import com.sabd.project2.output.OutputQueries;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

import java.util.ArrayList;

public class mapResultsKAFKA implements MapFunction<Tuple2<Long, ArrayList<Long>>, String> {

    private String type;

    public mapResultsKAFKA(String type){
        this.type=type;
    }

    @Override
    public String map(Tuple2<Long, ArrayList<Long>> tuplaInput) throws Exception {
        Gson gson = new Gson();
        String finalValue = Long.toString(tuplaInput.f0)+",";
        for (long x : tuplaInput.f1) {
            finalValue = finalValue.concat(Long.toString(x) + ",");
        }
        finalValue = finalValue.substring(0, finalValue.length() - 1);
        OutputQueries out = new OutputQueries(type, finalValue);

        //Parsing GSON
        finalValue = gson.toJson(out);
        return finalValue;
    }
}
