package com.sabd.project2.query2methods;

import com.google.gson.Gson;
import com.sabd.project2.output.OutputQueries;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import java.util.ArrayList;

public class mapResultsKAFKA implements MapFunction<ArrayList<Tuple3<Long, Long, Long>>, String> {

    private String type;

    public mapResultsKAFKA(String type){
        this.type = type;
    }

    @Override
    public String map(ArrayList<Tuple3<Long, Long, Long>> input) throws Exception {
        Gson gson = new Gson();

        String finalValue = "";
        int i = 0;
        Long minTimestamp = Long.MAX_VALUE;

        for (Tuple3<Long, Long, Long> x : input) {

            if(x.f2 < minTimestamp){
                minTimestamp = x.f2;
            }

            finalValue = finalValue.concat(Long.toString(x.f0) + "," + Long.toString(x.f1) + ",");
            if (i >= 9) {
                break;
            }
            i++;
        }

        finalValue = finalValue.substring(0, finalValue.length() - 1);
        String result =  Long.toString(minTimestamp) + "," + finalValue;

        OutputQueries out = new OutputQueries(type, result);

        //Parsing GSON
        return gson.toJson(out);

    }
}
