package com.sabd.project2.query2methods;

import com.sabd.project2.util.Tools;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.TreeSet;

public class applyClassifica implements AllWindowFunction<Tuple3<Long, Long, Long>, ArrayList<Tuple3<Long, Long, Long>>, TimeWindow> {

    @Override
    public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<ArrayList<Tuple3<Long, Long, Long>>> collector) throws Exception {
        TreeSet<Tuple3<Long, Long, Long>> tree = new TreeSet<>(Tools.compareQuery23);

        for (Tuple3<Long, Long, Long> obj : iterable) {
            tree.add(obj);
        }
        collector.collect(new ArrayList<>(tree));
    }
}
