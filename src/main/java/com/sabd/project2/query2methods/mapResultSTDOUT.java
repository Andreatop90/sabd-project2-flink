package com.sabd.project2.query2methods;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;

import java.util.ArrayList;

public class mapResultSTDOUT implements MapFunction<ArrayList<Tuple3<Long, Long, Long>>, String> {
    @Override
    public String map(ArrayList<Tuple3<Long, Long, Long>> input) throws Exception {

        String finalValue = "";
        int i = 0;
        Long minTimestamp = Long.MAX_VALUE;

        for (Tuple3<Long, Long, Long> x : input) {

            if(x.f2 < minTimestamp){
                minTimestamp = x.f2;
            }

            finalValue = finalValue.concat(Long.toString(x.f0) +","+ Long.toString(x.f1)+",");
            if (i >= 9) {
                break;
            }
            i++;
        }
        finalValue = finalValue.substring(0, finalValue.length() - 1);
        return Long.toString(minTimestamp) + "," + finalValue;
    }
}
