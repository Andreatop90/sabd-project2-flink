package com.sabd.project2;

import com.sabd.project2.Queries.Query1;
import com.sabd.project2.Queries.Query2;
import com.sabd.project2.Queries.Query3;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;

public class Start {

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.getConfig().setAutoWatermarkInterval(1000);

        FlinkKafkaProducer010<String> producerQuery1= new FlinkKafkaProducer010<>(
                Global.BROKER1, Global.TOPIC_QUERY1, new SimpleStringSchema());
        producerQuery1.setLogFailuresOnly(false);
        producerQuery1.setFlushOnCheckpoint(true);

        FlinkKafkaProducer010<String> producerQuery2= new FlinkKafkaProducer010<>(
                Global.BROKER2, Global.TOPIC_QUERY2, new SimpleStringSchema());
        producerQuery2.setLogFailuresOnly(false);
        producerQuery2.setFlushOnCheckpoint(true);

        FlinkKafkaProducer010<String> producerQuery3= new FlinkKafkaProducer010<>(
                Global.BROKER3, Global.TOPIC_QUERY3, new SimpleStringSchema());
        producerQuery3.setLogFailuresOnly(false);
        producerQuery3.setFlushOnCheckpoint(true);

        Query1.runQuery1(env,producerQuery1);
        Query2.runQuery2(env,producerQuery2);
        Query3.runQuery3(env, producerQuery3);
        env.execute("sabd-Project2");

    }
}

