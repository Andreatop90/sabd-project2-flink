package com.sabd.project2;

public class Global {

    //Kafka Configuration
    public static String BROKER1 ="172.20.0.6:9094,172.20.0.5:9093,172.20.0.4:9092";
    public static String BROKER2 ="172.20.0.6:9094,172.20.0.5:9093,172.20.0.4:9092";
    public static String BROKER3 ="172.20.0.6:9094,172.20.0.5:9093,172.20.0.4:9092";
    public static String ZOOKEEPER = "172.20.0.3:2181";

    //Topic degli streams da leggere in input
    public static String TOPIC_CONSUMER_COMMENTS = "topicComments";
    public static String TOPIC_CONSUMER_FRIENDSHIP = "topicFriendship";
    public static String TOPIC_CONSUMER_POSTS = "topicPosts";

    //Variabili di verifica sull'uso di Kafka in input o in output
    public static boolean USE_KAFKA_INPUT = false;
    public static boolean USE_KAFKA_OUTPUT= false;

    //FileName stream
    public static String FILENAME_COMMENTS = "comments.dat";
    public static String FILENAME_FRIENDSHIP = "friendships.dat";
    public static String FILENAME_POSTS = "posts.dat";

    //Topic consumer Output
    public static String TOPIC_QUERY1 = "topicQuery1";
    public static String TOPIC_QUERY2 = "topicQuery2";
    public static String TOPIC_QUERY3 = "topicQuery3";
}
