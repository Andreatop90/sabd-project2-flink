package com.sabd.project2.kafka;

import com.sabd.project2.Global;

import java.util.Properties;

public class ConsumerProperties {

    public static Properties getProperties(String broker){
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", broker);
        properties.setProperty("zookeeper.connect", Global.ZOOKEEPER);
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return properties;
    }
}
