package com.sabd.project2.state;

import com.sabd.project2.util.Tools;
import org.apache.flink.api.common.functions.RichAggregateFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

import java.util.ArrayList;

public class myStateFlatMap extends RichFlatMapFunction<Tuple3<Long, ArrayList<Long>,Integer>,Tuple2<Long, ArrayList<Long>>> {
    /**
     * The ValueState handle. The first field is the count, the second field a running sum.
     */
    private transient ValueState<Tuple3<Long, ArrayList<Long>, Integer>> sum;

    @Override
    public void open(Configuration config) {
        ArrayList<Long> array = Tools.initizialeArrayList(24);
        ValueStateDescriptor<Tuple3<Long, ArrayList<Long>, Integer>> descriptor =
                new ValueStateDescriptor<>(
                        "sumQuery1", // the state name
                        TypeInformation.of(new TypeHint<Tuple3<Long, ArrayList<Long>, Integer>>() {
                        }), // type information
                        Tuple3.of(0L, array, 0)); // default value of the state, if nothing was set
        sum = getRuntimeContext().getState(descriptor);
    }

    @Override
    public void flatMap(Tuple3<Long, ArrayList<Long>, Integer> input, Collector<Tuple2<Long, ArrayList<Long>>> out) throws Exception {
        // access the state value
        Tuple3<Long, ArrayList<Long>, Integer> storedValue = sum.value();

        storedValue.f1 = Tools.sumArrayList(storedValue.f1, input.f1);

        // update the state
        sum.update(new Tuple3<>(0L, storedValue.f1, storedValue.f2 + 1));

        // if the count reaches 2, emit the average and clear the state
        if (storedValue.f2 > 3) {
            out.collect(new Tuple2<>(0L, storedValue.f1));
            //UPDATE SOLO UN VALORE
            sum.update(new Tuple3<>(0L, storedValue.f1, 0));
        }
    }
}
