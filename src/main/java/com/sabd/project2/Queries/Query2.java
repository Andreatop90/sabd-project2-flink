package com.sabd.project2.Queries;

import com.sabd.project2.Global;
import com.sabd.project2.Start;
import com.sabd.project2.kafka.ConsumerProperties;
import com.sabd.project2.model.Comment;
import com.sabd.project2.query2methods.mapResultSTDOUT;
import com.sabd.project2.query2methods.mapResultsKAFKA;
import com.sabd.project2.util.Tools;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Query2 {

    public static void runQuery2(StreamExecutionEnvironment env, FlinkKafkaProducer010<String> producerQuery2) {


        //Instanziazione dataset iniziale
        DataStream<Comment> streamComment;
        if (Global.USE_KAFKA_INPUT) {

            // Consumer kafka Comments
            FlinkKafkaConsumer010<String> commentsConsumer = new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_COMMENTS,
                    new SimpleStringSchema(), ConsumerProperties.getProperties(Global.BROKER2));

            //filtro le tuple non conformi
            streamComment = env.addSource(commentsConsumer)

                    //Creazione dell'oggetto Comment
                    .map(Comment::createObjComment)
                    //Filtraggio dati in base al campo Condition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Comment::getCondition)
                    //Filtraggio dati in base al campo getDirect che sarà impostato a false se il commento non era diretto
                    .filter(Comment::getDirect)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Comment>() {
                        @Override
                        public long extractAscendingTimestamp(Comment comment) {

                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(comment.getTimestamp());
                        }
                    });
        } else {
            //Lettura da File per i test, in questo caso non viene introdotta latency di comunicazione
            ClassLoader resourceLoader = Start.class.getClassLoader();

            //Lettura da file dello stream comment.dat
            streamComment = env.readTextFile(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_COMMENTS)).getFile())

                    //Creazione dell'oggetto Comment
                    .map(Comment::createObjComment)
                    //Filtraggio dati in base al campo getDirect che sarà impostato a false se il commento non era diretto
                    .filter(Comment::getDirect)
                    //Filtraggio dati in base al campo getCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Comment::getCondition)
                    //Filtraggio dati in base al campo getDirect che sarà impostato a false se il commento non era diretto
                    .filter(Comment::getDirect)
                    //Assegnazione di un timeStamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Comment>() {
                        @Override
                        public long extractAscendingTimestamp(Comment comment) {

                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(comment.getTimestamp());
                        }
                    });
        }

        //Map dell'oggetto comment , mantenendo solo l'ID del post di riferimento e il valore 1 per le successive somme delle tuple
        DataStream<Tuple3<Long, Long, Long>> comments = streamComment.map(new MapFunction<Comment, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(Comment comment) throws Exception {
                return new Tuple2<>(comment.getPostId(), 1L);
            }
        })      .keyBy(0)
                .timeWindow(Time.hours(1))
                .apply(new WindowFunction<Tuple2<Long, Long>, Tuple3<Long, Long, Long>, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple2<Long, Long>> iterable, Collector<Tuple3<Long, Long, Long>> collector) throws Exception {
                        Long total = 0L;
                        Long postID = 0L;
                        for (Tuple2<Long, Long> obj : iterable) {
                            total += obj.f1;
                            postID = obj.f0;
                        }
                        collector.collect(new Tuple3<>(postID, total, timeWindow.getStart()));
                    }
                });

        //Accorpo risultati in finestre di un ora riunendo le chiavi precedentemente partizionate, ordinamento effettuato mediante uso di TreeSet
        DataStream<ArrayList<Tuple3<Long, Long, Long>>> comments1Hour = comments.timeWindowAll(Time.hours(1)).apply(new AllWindowFunction<Tuple3<Long, Long, Long>, ArrayList<Tuple3<Long, Long, Long>>, TimeWindow>() {
            @Override
            public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<ArrayList<Tuple3<Long, Long, Long>>> collector) throws Exception {
                TreeSet<Tuple3<Long, Long, Long>> tree = new TreeSet<>(Tools.compareQuery23);

                for (Tuple3<Long, Long, Long> obj : iterable) {
                    tree.add(obj);
                }
                collector.collect(new ArrayList<>(tree));
            }
        });
        //Somma dei valori accorpando per 24 i valori precedentemente raccolti per un'ora nel datastream comments
        DataStream<Tuple3<Long, Long, Long>> sum24Hours = comments.keyBy(0).timeWindow(Time.hours(24)).sum(1);

        //Riaggregazione dei valori di 24 ore
        DataStream<ArrayList<Tuple3<Long, Long, Long>>> comment24Hours = sum24Hours.timeWindowAll(Time.hours(24)).apply(new AllWindowFunction<Tuple3<Long, Long, Long>, ArrayList<Tuple3<Long, Long, Long>>, TimeWindow>() {
            @Override
            public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<ArrayList<Tuple3<Long, Long, Long>>> collector) throws Exception {
                TreeSet<Tuple3<Long, Long, Long>> tree = new TreeSet<>(Tools.compareQuery23);

                for (Tuple3<Long, Long, Long> obj : iterable) {
                    tree.add(obj);
                }
                collector.collect(new ArrayList<>(tree));
            }
        });

        //Effettuo le stesse 2 operazioni precedenti stavolta su una finestra di 7 giorni
        DataStream<Tuple3<Long, Long, Long>> sum7Days = comments.keyBy(0).timeWindow(Time.days(7)).sum(1);
        DataStream<ArrayList<Tuple3<Long, Long, Long>>> comment7Days = sum7Days.timeWindowAll(Time.days(7)).apply(new AllWindowFunction<Tuple3<Long, Long, Long>, ArrayList<Tuple3<Long, Long, Long>>, TimeWindow>() {
            @Override
            public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<ArrayList<Tuple3<Long, Long, Long>>> collector) throws Exception {
                TreeSet<Tuple3<Long, Long, Long>> tree = new TreeSet<>(Tools.compareQuery23);

                for (Tuple3<Long, Long, Long> obj : iterable) {
                    tree.add(obj);
                }
                collector.collect(new ArrayList<>(tree));
            }
        });

        if (Global.USE_KAFKA_OUTPUT) {

            //Formattazione dell'output per kafka
            DataStream<String> printComments1Hours = comments1Hour.map(new mapResultsKAFKA("type1"));
            DataStream<String> printComments24Hours = comment24Hours.map(new mapResultsKAFKA("type2"));
            DataStream<String> printComments7Days = comment7Days.map(new mapResultsKAFKA("type3"));

            printComments1Hours.addSink(producerQuery2);
            printComments24Hours.addSink(producerQuery2);
            printComments7Days.addSink(producerQuery2);

        } else {

            //Formattazione dell'output per STDOUT
            DataStream<String> printComments1Hours = comments1Hour.map(new mapResultSTDOUT());
            DataStream<String> printComments24Hours = comment24Hours.map(new mapResultSTDOUT());
            DataStream<String> printComments7Days = comment7Days.map(new mapResultSTDOUT());

            printComments1Hours.print().setParallelism(1);
            printComments24Hours.print().setParallelism(1);
            printComments7Days.print().setParallelism(1);
        }


    }

}
