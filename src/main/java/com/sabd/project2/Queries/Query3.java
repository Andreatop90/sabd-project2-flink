package com.sabd.project2.Queries;


import com.sabd.project2.Global;
import com.sabd.project2.Start;
import com.sabd.project2.kafka.ConsumerProperties;
import com.sabd.project2.model.Comment;
import com.sabd.project2.model.Friendship;
import com.sabd.project2.model.Post;
import com.sabd.project2.query2methods.applyClassifica;
import com.sabd.project2.query2methods.mapResultSTDOUT;
import com.sabd.project2.query2methods.mapResultsKAFKA;
import com.sabd.project2.util.Tools;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Query3 {


    public static void runQuery3(StreamExecutionEnvironment env, FlinkKafkaProducer010<String> producerQuery3){

        DataStream<Comment> streamComment;
        DataStream<Post> streamPost;
        DataStream<Friendship> streamFriendship;

        if (Global.USE_KAFKA_INPUT) {

            // Consumer kafka
            FlinkKafkaConsumer010<String> commentsConsumer = new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_COMMENTS,
                    new SimpleStringSchema(), ConsumerProperties.getProperties(Global.BROKER3));
            FlinkKafkaConsumer010<String> friendshipConsumer = new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_FRIENDSHIP,
                    new SimpleStringSchema(), ConsumerProperties.getProperties(Global.BROKER3));
            FlinkKafkaConsumer010<String> postConsumer = new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_POSTS,
                    new SimpleStringSchema(), ConsumerProperties.getProperties(Global.BROKER3));

            //filtro le tuple non conformi
            streamComment = env.addSource(commentsConsumer)
                    //Creazione dell'oggetto Comment
                    .map(Comment::createObjComment)
                    //Filtraggio dati in base al campo Condition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Comment::getCondition)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Comment>() {
                        @Override
                        public long extractAscendingTimestamp(Comment comment) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(comment.getTimestamp());
                        }
                    });
            streamFriendship = env.addSource(friendshipConsumer)
                    //Creazione dell'oggetto Friendship
                    .map(Friendship::createObjFriendship)
                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Friendship::isCondition)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Friendship>() {
                        @Override
                        public long extractAscendingTimestamp(Friendship friendship) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(friendship.getTimestamp());
                        }
                    });
            streamPost = env.addSource(postConsumer)
                    //Creazione dell'oggetto Post
                    .map(Post::createObjPost)
                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Post::isCondition)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Post>() {
                        @Override
                        public long extractAscendingTimestamp(Post post) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(post.getTimestamp());
                        }
                    });


        } else {
            //Lettura da File per i test, in questo caso non viene introdotta latency di comunicazione
            ClassLoader resourceLoader = Start.class.getClassLoader();

            //Lettura da file dello stream comment.dat
            streamComment = env.readTextFile(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_COMMENTS)).getFile())
                    //Creazione dell'oggetto Comment
                    .map(Comment::createObjComment)
                    //Filtraggio dati in base al campo getCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Comment::getCondition)
                    //Assegnazione di un timeStamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Comment>() {
                        @Override
                        public long extractAscendingTimestamp(Comment comment) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(comment.getTimestamp());
                        }
                    });
            streamFriendship = env.readTextFile(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_FRIENDSHIP)).getFile())
                    //Creazione dell'oggetto Friendship
                    .map(Friendship::createObjFriendship)
                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Friendship::isCondition)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Friendship>() {
                        @Override
                        public long extractAscendingTimestamp(Friendship friendship) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(friendship.getTimestamp());
                        }
                    });
            streamPost = env.readTextFile(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_POSTS)).getFile())
                    //Creazione dell'oggetto Post
                    .map(Post::createObjPost)
                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Post::isCondition)
                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Post>() {
                        @Override
                        public long extractAscendingTimestamp(Post post) {
                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(post.getTimestamp());
                        }
                    });
        }


        // Mappo i commenti e inizio a contare
        DataStream<Tuple2< Long,Long>> commentCounter = streamComment.map(new MapFunction<Comment, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(Comment comment) throws Exception {
                return new Tuple2<>(comment.getUserId(), 1L);
            }
        });

        // Mappo i post e inizio a contare
        DataStream<Tuple2<Long, Long>> postCounter = streamPost.map(new MapFunction<Post, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(Post post) throws Exception {
                return new Tuple2<>(post.getUserId(), 1L);
            }
        });

        // Mappo le friendship e inizio a contare gli utenti nella prima posizione
        DataStream<Tuple2<Long, Long>> friendshipCounter1 = streamFriendship.map(new MapFunction<Friendship, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(Friendship friendship) throws Exception {
                return new Tuple2<>(friendship.getUser1(), 1L);
            }
        });

        // Mappo le friendship e inizio a contare gli utenti nella seconda posizione
        DataStream<Tuple2< Long, Long>> friendshipCounter2 = streamFriendship.map(new MapFunction<Friendship, Tuple2<Long, Long>>() {
            @Override
            public Tuple2<Long, Long> map(Friendship friendship) throws Exception {
                return new Tuple2<>(friendship.getUser2(), 1L);
            }
        });

        // Concateno tutti i datastream
        DataStream<Tuple3<Long, Long, Long>> combined = commentCounter.union(postCounter, friendshipCounter1, friendshipCounter2)
                .keyBy(0)
                .timeWindow(Time.hours(1))
                .apply(new WindowFunction<Tuple2< Long, Long>, Tuple3<Long, Long, Long>, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple2< Long, Long>> iterable, Collector<Tuple3<Long, Long, Long>> collector) throws Exception {
                        Long total = 0L;
                        Long id = 0L;

                        for(Tuple2<Long, Long> obj : iterable){
                            total += obj.f1;
                            id = obj.f0;
                        }
                        collector.collect(new Tuple3<>(id, total,timeWindow.getStart()));
                    }
                });

        // Window con classifica per 1 ora
        DataStream<ArrayList<Tuple3<Long, Long, Long>>> combinedHourResult = combined.timeWindowAll(Time.hours(1))
                .apply(new applyClassifica());


        // Window con classifica per 24 ore
        DataStream<Tuple3<Long, Long, Long>> combinedDay = combined.keyBy(0)
                .timeWindow(Time.hours(24))
                .apply(new WindowFunction<Tuple3<Long, Long, Long>, Tuple3<Long, Long, Long>, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<Tuple3<Long, Long, Long>> collector) throws Exception {
                        Long total = 0L;
                        Long id = 0L;

                        for(Tuple3<Long, Long, Long> obj : iterable){
                            total += obj.f1;
                            id = obj.f0;
                        }

                        collector.collect(new Tuple3<>(id, total, timeWindow.getStart()));
                    }
                });

        DataStream<ArrayList<Tuple3<Long, Long, Long>>> combinedDayResult = combinedDay.timeWindowAll(Time.hours(24))
                .apply(new applyClassifica());

        // Window con classifica per 7 giorni
        DataStream<Tuple3<Long, Long, Long>> combinedWeek = combined.keyBy(0)
                .timeWindow(Time.days(7))
                .apply(new WindowFunction<Tuple3<Long, Long, Long>, Tuple3<Long, Long, Long>, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<Tuple3<Long, Long, Long>> collector) throws Exception {
                        Long total = 0L;
                        Long id = 0L;

                        for(Tuple3<Long, Long, Long> obj : iterable){
                            total += obj.f1;
                            id = obj.f0;
                        }

                        collector.collect(new Tuple3<>(id, total, timeWindow.getStart()));
                    }
                });

        DataStream<ArrayList<Tuple3<Long, Long, Long>>> combinedWeekResult = combinedWeek.timeWindowAll(Time.days(7))
                .apply(new applyClassifica());


        if (Global.USE_KAFKA_OUTPUT) {
            // redirect output su Kafka
            DataStream<String> printCombined = combinedHourResult.map(new mapResultsKAFKA("type1"));
            DataStream<String> printCombinedDay = combinedDayResult.map(new mapResultsKAFKA("type2"));
            DataStream<String> printCombinedWeek = combinedWeekResult.map(new mapResultsKAFKA("type3"));

            printCombined.addSink(producerQuery3);
            printCombinedDay.addSink(producerQuery3);
            printCombinedWeek.addSink(producerQuery3);

        } else {
            // Stampare output a schermo
            DataStream<String> printCombined = combinedHourResult.map(new mapResultSTDOUT());
            DataStream<String> printCombinedDay = combinedDayResult.map(new mapResultSTDOUT());
            DataStream<String> printCombinedWeek = combinedWeekResult.map(new mapResultSTDOUT());

            printCombined.print().setParallelism(1);
            printCombinedDay.print().setParallelism(1);
            printCombinedWeek.print().setParallelism(1);
        }
    }

}
