package com.sabd.project2.Queries;

import com.google.gson.Gson;
import com.sabd.project2.Global;
import com.sabd.project2.Start;
import com.sabd.project2.kafka.ConsumerProperties;
import com.sabd.project2.model.Friendship;
import com.sabd.project2.output.OutputQueries;
import com.sabd.project2.query1methods.mapResultsKAFKA;
import com.sabd.project2.state.myStateFlatMap;
import com.sabd.project2.util.Tools;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class Query1 {

    public static void runQuery1(StreamExecutionEnvironment env, FlinkKafkaProducer010<String> producerQuery1) {



        //Instanziazione dataset iniziale
        DataStream<Friendship> streamFriendship;
        if (Global.USE_KAFKA_INPUT) {

            // Consumer kafka Friendship
            FlinkKafkaConsumer010<String> friendshipConsumer = new FlinkKafkaConsumer010<String>(Global.TOPIC_CONSUMER_FRIENDSHIP,
                    new SimpleStringSchema(), ConsumerProperties.getProperties(Global.BROKER1));

            //filtro le tuple non conformi
            streamFriendship = env.addSource(friendshipConsumer)

                    //Creazione dell'oggetto Friendship
                    .map(Friendship::createObjFriendship)

                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Friendship::isCondition)

                    //Assegnazione di un timestamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Friendship>() {
                        @Override
                        public long extractAscendingTimestamp(Friendship friendship) {

                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(friendship.getTimestamp());
                        }

                    });
        } else {
            //Lettura da File per i test, in questo caso non viene introdotta latency di comunicazione

            ClassLoader resourceLoader = Start.class.getClassLoader();
            //Lettura da file dello stream friendships.dat
            streamFriendship = env.readTextFile(Objects.requireNonNull(resourceLoader.getResource(Global.FILENAME_FRIENDSHIP)).getFile())

                    //Creazione dell'oggetto Friendship
                    .map(Friendship::createObjFriendship)

                    //Filtraggio dati in base al campo isCondition che sarà impostato a false se i dati ricevuti non erano completi
                    .filter(Friendship::isCondition)

                    //Assegnazione di un timeStamp per lavorare con gli eventi
                    .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Friendship>() {
                        @Override
                        public long extractAscendingTimestamp(Friendship friendship) {

                            //Il timestamp espresso in secondi viene riconvertito in millisecondi per Flink
                            return TimeUnit.SECONDS.toMillis(friendship.getTimestamp());
                        }
                    });
        }


        //Datastream composto da una tupla3<Timeslot,TimeStamp,1>
        DataStream<Tuple3<Integer, Long, Long>> tupleFriendShips = streamFriendship.map(new MapFunction<Friendship, Tuple5<Integer, Long, Long,Long,Long>>() {
            @Override
            public Tuple5<Integer, Long, Long,Long,Long> map(Friendship f) throws Exception {
                return new Tuple5<>(Tools.assignTimeslot(f.getTimestamp()), f.getTimestamp(), 1L,f.getUser1(),f.getUser2());
            }
        })
                //Raggruppo le Tuple in base alla fascia oraria
                .keyBy(0).timeWindow(Time.days(1))

                //Applico una funzione apply che si occupa di sommare i contributi di amicizia per ogni fascia
                .apply(new WindowFunction<Tuple5<Integer, Long, Long, Long, Long>, Tuple3<Integer, Long, Long>, Tuple, TimeWindow>() {
                    @Override
                    public void apply(Tuple tuple, TimeWindow timeWindow, Iterable<Tuple5<Integer, Long, Long, Long, Long>> iterable, Collector<Tuple3<Integer, Long, Long>> collector) throws Exception {
                        Integer fascia = 0;
                        TreeSet<Tuple5<Integer, Long, Long,Long,Long>> tree = new TreeSet<>(Tools.compareQuery1);

                        for (Tuple5<Integer, Long, Long,Long,Long> obj : iterable) {
                            tree.add(obj);
                            fascia = obj.f0;
                        }

                        collector.collect(new Tuple3<>(fascia, timeWindow.getStart(), (long) tree.size()));
                    }
                });

        //Creo una finestra per riunire le Key analizzate singolarmente, in modo da poter costruire l'output nella forma richiesta
        DataStream<Tuple2<Long, ArrayList<Long>>> result = tupleFriendShips.windowAll(TumblingEventTimeWindows.of(Time.days(1)))
                .apply(new AllWindowFunction<Tuple3<Integer, Long, Long>, Tuple2<Long, ArrayList<Long>>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple3<Integer, Long, Long>> iterable,
                                      Collector<Tuple2<Long, ArrayList<Long>>> collector) throws Exception {

                        ArrayList<Long> result = Tools.initizialeArrayList(24);

                        for (Tuple3<Integer, Long, Long> obj : iterable) {
                            result.set(obj.f0, result.get(obj.f0) + obj.f2);
                        }

                        collector.collect(new Tuple2<>(timeWindow.getStart(), result));
                    }
                });

        //Calcolo dei valori settimanali a partire da quelli giornalieri
        DataStream<Tuple2<Long, ArrayList<Long>>> resultWeek = result.windowAll(TumblingEventTimeWindows.of(Time.days(7)))
                .apply(new AllWindowFunction<Tuple2<Long, ArrayList<Long>>, Tuple2<Long, ArrayList<Long>>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple2<Long, ArrayList<Long>>> iterable,
                                      Collector<Tuple2<Long, ArrayList<Long>>> collector) throws Exception {

                        ArrayList<Long> result = Tools.initizialeArrayList(24);


                        for (Tuple2<Long, ArrayList<Long>> obj : iterable) {
                            result = Tools.sumArrayList(result, obj.f1);
                        }

                        collector.collect(new Tuple2<>(timeWindow.getStart(), result));

                    }
                });

        //Calcolo del punto 3 della Query 1 utilizzando un operatore statefull
        DataStream<Tuple2<Long,ArrayList<Long>>> resultAll = resultWeek.map(new MapFunction<Tuple2<Long, ArrayList<Long>>, Tuple3<Long, ArrayList<Long>,Integer>>() {
            @Override
            public Tuple3<Long, ArrayList<Long>,Integer> map(Tuple2<Long, ArrayList<Long>> input) throws Exception {
                return new Tuple3<>(input.f0,input.f1,0);
            }
        }).keyBy(2).flatMap(new myStateFlatMap());



        //TODO manca l'ultima finestra da chiedere alla professoressa


        //Print a schermo dei risultati
        if(Global.USE_KAFKA_OUTPUT){

            //Dataset trasformato per consentire l'invio su Kafka di una stringa disposta come richiesto
            DataStream<String> printResult = result.map(new mapResultsKAFKA("type1"));

            //Dataset trasformato per consentire l'invio su Kafka di una stringa disposta come richiesto
            DataStream<String> printResultWeek = resultWeek.map(new mapResultsKAFKA("type2"));

            //Dataset trasformato per consentire l'invio su Kafka di una stringa disposta come richiesto
            DataStream<String> printResultAll = resultAll.map(new MapFunction<Tuple2<Long, ArrayList<Long>>, String>() {
                @Override
                public String map(Tuple2<Long, ArrayList<Long>> tuplaInput) throws Exception {
                    Gson gson = new Gson();
                    String finalValue = Long.toString(tuplaInput.f0)+",";
                    for (long x : tuplaInput.f1) {
                        finalValue = finalValue.concat(Long.toString(x) + ",");
                    }
                    finalValue = finalValue.substring(0, finalValue.length() - 1);
                    OutputQueries out = new OutputQueries("type3", finalValue);

                    //Parsing GSON
                    finalValue = gson.toJson(out);
                    return finalValue;
                }
            });



            //Invio risultati su Kafka
            printResult.addSink(producerQuery1);
            printResultWeek.addSink(producerQuery1);
            printResultAll.addSink(producerQuery1);
        }else{
            //Invio risultati su STDOUT
            result.print().setParallelism(1);
            resultWeek.print().setParallelism(1);
            resultAll.print().setParallelism(1);
        }

    }


}
