package com.sabd.project2.model;

import com.sabd.project2.util.Tools;

import java.text.ParseException;

public class Comment {

    private Long timestamp;
    private Long commentId;
    private Long userId;
    private String comment;
    private String user;
    private Long commentReplyId;
    private Long postId;
    private Boolean condition = true;
    private Boolean direct = false;

    public Comment() { super();}

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getCommentReplyId() {
        return commentReplyId;
    }

    public void setCommentReplyId(Long commentReplyId) {
        this.commentReplyId = commentReplyId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Boolean getCondition() {
        return condition;
    }

    public void setCondition(Boolean condition) {
        this.condition = condition;
    }

    public Boolean getDirect() {
        return direct;
    }

    public void setDirect(Boolean direct) {
        this.direct = direct;
    }

    public static Comment createObjComment(String s) throws ParseException {

        String[] parts = s.split("\\|");
        Comment comment = new Comment();
        comment.setTimestamp(Tools.getLongDate(parts[0]));

        if(!parts[1].equals("")){
            comment.setCommentId(Long.parseLong(parts[1]));
        }else{comment.setCondition(false);}
        if(!parts[2].equals("")){
            comment.setUserId(Long.parseLong(parts[2]));
        }else{comment.setCondition(false);}
        if(!parts[3].equals("")){
            comment.setComment(parts[3]);
        }else{comment.setCondition(false);}

        if(parts[5].equals("") && parts[6].equals("")){
            comment.setCondition(false);
        } else {
            if(!parts[5].equals("")){
                comment.setCommentReplyId(Long.parseLong(parts[5]));
                comment.setDirect(false);
            } else {
                comment.setPostId(Long.parseLong(parts[6]));
                comment.setDirect(true);
            }
        }

        return comment;
    }
}
