package com.sabd.project2.model;

import com.sabd.project2.util.Tools;

import java.text.ParseException;

public class Post {

    private Long timestamp;
    private Long userId;
    private Long postId;
    private String post;
    private String user;
    private Boolean condition = true;

    public Post(){
        super();
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public boolean isCondition() {
        return condition;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }


    public static Post createObjPost(String s) throws ParseException {

        String[] parts = s.split("\\|");

        Post post = new Post();
        post.setTimestamp(Tools.getLongDate(parts[0]));

        if(!parts[1].equals("")){
            post.setPostId(Long.parseLong(parts[1]));

        }else {post.setCondition(false);}

        if(!parts[2].equals("")){
            post.setUserId(Long.parseLong(parts[2]));

        }else {post.setCondition(false);}

        if(!parts[3].equals("")){
            post.setPost(parts[3]);

        }else {post.setCondition(false);}

        if(!parts[4].equals("")){
            post.setUser(parts[4]);

        } else {post.setCondition(false);}

        return post;
    }
}
