package com.sabd.project2.model;

import com.sabd.project2.util.Tools;

import java.text.ParseException;

public class Friendship {

    private long timestamp;
    private long user1;
    private long user2;

    private boolean condition=true;

    public Friendship(long timestamp, int user1, int user2) {
        this.timestamp = timestamp;
        this.user1 = user1;
        this.user2 = user2;
    }

    public Friendship(){
        super();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getUser1() {
        return user1;
    }

    public void setUser1(long user1) {
        this.user1 = user1;
    }

    public long getUser2() {
        return user2;
    }

    public void setUser2(long user2) {
        this.user2 = user2;
    }

    public boolean isCondition() {
        return condition;
    }

    public void setCondition(boolean condition) {
        this.condition = condition;
    }


    public static Friendship createObjFriendship(String s) throws ParseException {
        String[] parts = s.split("\\|");
        Friendship friend = new Friendship();
        long timestamp = Tools.getLongDate(parts[0]);
        friend.setTimestamp(timestamp);
        long user1 = 0;
        long user2 = 0;
        if(!parts[1].equals("")){
            user1 = Long.parseLong(parts[1]);
            friend.setUser1(user1);
        }else {
            friend.setCondition(false);
        }
        if(!parts[2].equals("")){
            user2 = Long.parseLong(parts[2]);
            friend.setUser2(user2);
        }else{
            friend.setCondition(false);
        }
        return friend;
    }
}
