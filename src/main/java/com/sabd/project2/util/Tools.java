package com.sabd.project2.util;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;



public class Tools {

    public static Long getLongDate(String s) throws ParseException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        LocalDateTime triggerTime = LocalDateTime.parse(s, formatter);
        ZoneId zoneUTC = ZoneId.of("UTC");

        return triggerTime.atZone(zoneUTC).toEpochSecond();
    }

    public static int assignTimeslot(Long timestamp){

        LocalDateTime localdate = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneId.of("UTC"));

        return localdate.getHour();
    }


    public static ArrayList<Long> initizialeArrayList(int size){

        ArrayList<Long> result = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            result.add(0L);
        }

        return result;
    }


    public static ArrayList<Long> sumArrayList(ArrayList<Long> a, ArrayList<Long> b){

        ArrayList<Long> result = initizialeArrayList(24);

        for(int i=0; i<a.size(); i++){
            result.set(i, a.get(i) + b.get(i));
        }

        return result;

    }

    public static Comparator<Tuple5<Integer, Long, Long,Long,Long>> compareQuery1 =
            (Tuple5<Integer, Long, Long,Long,Long> o1, Tuple5<Integer, Long, Long,Long,Long> o2) -> {

            if (o1.f3.equals(o2.f4)&&o1.f4.equals(o2.f3)) {
                return 0;
            }else
                return 1;
            };

    public static Comparator<Tuple3<Long,Long,Long>> compareQuery23 =
            (Tuple3<Long,Long,Long> t1, Tuple3<Long,Long,Long> t2) -> {

                if (t1.f1 < t2.f1) {
                    return 1;
                }
                else if (t1.f1.equals(t2.f1)) {
                    if(t1.f0<t2.f0){
                        return 1;
                    } else {
                        return -1;
                    }
                }
                else if(t1.f1.equals(t2.f1)&&t1.f0<t2.f0){
                    return 0;
                }
                else
                    return -1;
            };


    public static void main(String[] args) throws ParseException {


        System.out.println(getLongDate("2010-02-03T16:35:50.015+0000"));

        System.out.println(assignTimeslot(1265214950L));
    }

}
